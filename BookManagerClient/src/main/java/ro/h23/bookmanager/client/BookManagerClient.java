package ro.h23.bookmanager.client;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.h23.bookmanager.core.Book;

/**
 * Hello world!
 *
 */
public class BookManagerClient {

    private static Logger log = LoggerFactory.getLogger(BookManagerClient.class);

    private static URI getBaseURI() {
        //TODO change the port to whatever is the server running on
        return UriBuilder.fromUri("http://localhost:8080/BookManagerWebServices/").build();
    }

    private static void logResponse(String message, Response response) {
        log.info("{} - status: {} {}, body: \r\n{}", message, response.getStatus(), response.getStatusInfo().getReasonPhrase(),
                response.readEntity(String.class));
    }

    public static void main(String[] args) {
        ClientConfig config = new ClientConfig();
        //config.register(Custom);
        Client client = ClientBuilder.newClient(config);
        // Next line of code is a workaround for using PATCH
        // A value of true declares that the client will try to set unsupported HTTP method to java.net.HttpURLConnection via reflection.
        // PATCH workaround:
        //    - alternative to client.property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true);
        //    - also allow PATCH to have a response body
        //    - see user1648865 response from https://stackoverflow.com/questions/17897171/how-to-have-a-patch-annotation-for-jax-rs 
        client.property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true);
        //
        WebTarget service = client.target(getBaseURI());
        Response response;
        Book book;

        response = service.path("api").path("books").request().accept(MediaType.APPLICATION_JSON).get(Response.class);
        logResponse("Book collection", response);

        // create a book and added to the collection
        book = new Book("9786065796553", "Christine", "Stephen King", "Nemira");
        response = service.path("api").path("books").request(MediaType.APPLICATION_XML)
                .post(Entity.entity(book, MediaType.APPLICATION_XML), Response.class);
        // Return code should be 201 == created resource
        logResponse("Book added", response);

        response = service.path("api").path("books").request().accept(MediaType.APPLICATION_JSON).get(Response.class);
        logResponse("Book collection", response);

        book = new Book("9786065794412", "The Shining", "Stephen King", "Nemira");
        response = service.path("api").path("books").request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(book, MediaType.APPLICATION_JSON), Response.class);
        // Return code should be 201 == created resource
        logResponse("Book added", response);

        book = new Book("9786065794412", "The Shining", "Stephen King", "Nemira");
        response = service.path("api").path("books").request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(book, MediaType.APPLICATION_JSON), Response.class);
        // Return code should be 201 == created resource
        logResponse("Book added", response);

        response = service.path("api").path("books").path("9786065794412").request().accept(MediaType.APPLICATION_JSON)
                .get(Response.class);
        logResponse("Book collection", response);

        response = service.path("api").path("books").path("9786065794412").request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(book, MediaType.APPLICATION_JSON), Response.class);
        // Return code should be 201 == created resource
        logResponse("POST request on colection item is not allowed", response);

        response = service.path("api").path("books").request(MediaType.APPLICATION_JSON)
                .put(Entity.entity("", MediaType.APPLICATION_JSON), Response.class);
        logResponse("Replace entire book collection is not allowed", response);

        // PUT
        book = new Book("9786065794412", "Shining", "Stephen King", "Nemira");
        response = service.path("api").path("books").path("9786065794412").request(MediaType.APPLICATION_JSON)
                .put(Entity.entity(book, MediaType.APPLICATION_JSON), Response.class);
        logResponse("Replace book with ISBN 9786065794412", response);

        response = service.path("api").path("books").request().accept(MediaType.APPLICATION_JSON).get(Response.class);
        logResponse("Book collection", response);

        // DELETE
        response = service.path("api").path("books").request().accept(MediaType.APPLICATION_JSON).delete(Response.class);
        logResponse("DELETE request is not allowed", response);

        response = service.path("api").path("books").path("9786065796553").request().accept(MediaType.APPLICATION_JSON)
                .delete(Response.class);
        logResponse("Delete book with ISBN 9786065796553", response);

        response = service.path("api").path("books").request().accept(MediaType.APPLICATION_JSON).get(Response.class);
        logResponse("Book collection", response);

        //PATCH
        //header("X-HTTP-Method-Override", "PATCH") <-- does not work
        response = service.path("api").path("books").path("9786065794412").request().accept(MediaType.APPLICATION_JSON).method(
                "PATCH", Entity.entity("title=NewTitle&publisher=New+Publisher", MediaType.APPLICATION_FORM_URLENCODED),
                Response.class);
        logResponse("Patch book with ISBN 9786065794412; changed title", response);

        response = service.path("api").path("books").request().accept(MediaType.APPLICATION_JSON).get(Response.class);
        logResponse("Book collection", response);
    }
}
