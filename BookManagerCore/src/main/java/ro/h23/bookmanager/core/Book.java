/*
 * Copyright (C) 2018 Adrian Alexandrescu. All rights reserved.
 * ADRIAN ALEXANDRESCU PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * See <license.txt> for more details.
 */
package ro.h23.bookmanager.core;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Adrian
 * @created 24 feb. 2018
 * @version 1.0
 */
@XmlRootElement
public class Book {

    public static int MISSING_ID = -1;

    private int       id;
    private String    isbn;
    private String    title;
    private String    author;
    private String    publisher;
    private Date      addedDate;

    /**
     * !!! WARNING: A no argument constructor is required by the XML (de)serializer
     */
    public Book() {
    }

    /**
     * The book id is manually set when persisting the object
     * 
     * @param title
     * @param author
     * @param publisher
     * @param addedDate
     */
    public Book(String isbn, String title, String author, String publisher) {
        super();
        this.id = MISSING_ID;
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.addedDate = new Date();
    }

    /**
     * @param id
     * @param title
     * @param author
     * @param publisher
     * @param addedDate
     */
    public Book(int id, String isbn, String title, String author, String publisher, Date addedDate) {
        super();
        this.id = id;
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.addedDate = addedDate;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the isbn
     */
    public String getISBN() {
        return isbn;
    }

    /**
     * @param isbn
     *            the isbn to set
     */
    public void setISBN(String isbn) {
        this.isbn = isbn;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author
     *            the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return the publisher
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * @param publisher
     *            the publisher to set
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    /**
     * @return the addedDate
     */
    public Date getAddedDate() {
        return addedDate;
    }

    /**
     * @param addedDate
     *            the addedDate to set
     */
    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Book [id=").append(id).append(", isbn=").append(isbn).append(", title=").append(title).append(", author=")
                .append(author).append(", publisher=").append(publisher).append(", addedDate=").append(addedDate).append("]");
        return builder.toString();
    }
    
    

}
