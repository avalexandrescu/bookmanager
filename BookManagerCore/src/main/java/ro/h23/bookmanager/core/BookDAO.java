/*
 * Copyright (C) 2018 Adrian Alexandrescu. All rights reserved.
 * ADRIAN ALEXANDRESCU PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * See <license.txt> for more details.
 */
package ro.h23.bookmanager.core;

import java.util.List;

/**
 * @author Adrian
 * @created 25 feb. 2018
 * @version 1.0
 */
public interface BookDAO {

    List<Book> getBooks();

    Book findBookById(int id);

    Book findBookByISBN(String isbn);

    /**
     * 
     * @param book
     *            is updated with the id inside this method
     * @return <code>true</code> if the book was added successfully, <code>false</code> if the book
     *         already exists
     */
    boolean addBook(Book book);

    boolean updateBook(String isbn, Book newBook);

    boolean deleteBook(Book book);
    
    boolean deleteBookByISBN(String isbn);
}
