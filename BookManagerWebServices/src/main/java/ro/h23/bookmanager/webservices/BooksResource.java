/*
 * Copyright (C) 2018 Adrian Alexandrescu. All rights reserved.
 * ADRIAN ALEXANDRESCU PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * See <license.txt> for more details.
 */
package ro.h23.bookmanager.webservices;

import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
/**
 * @author Adrian
 * @created 25 feb. 2018
 * @version 1.0
 */
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.h23.bookmanager.core.Book;
import ro.h23.bookmanager.db.ListBookDAO;

// e.g., @ApplicationPath("api") + @Path("/books") =>
// http://localhost:8080/BookManagerWebServices/api/books
@Path("/books")
public class BooksResource {

    private static Logger log = LoggerFactory.getLogger(BooksResource.class);

    // based on: http://www.vogella.com/tutorials/REST/article.html#jerseyprojectsetup

    /**
     * Requests transfer of a current selected representation for the target resource (i.e., the
     * book collection)
     */
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<Book> getBooks() {
        log.info("getBooks");
        return ListBookDAO.instance().getBooks();
    }

    /**
     * Requests transfer of a current selected representation for the target resource (i.e., a book
     * by ISBN).
     */
    @GET
    @Path("{isbn}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Book getBook(@PathParam("isbn") String isbn) {
        return ListBookDAO.instance().findBookByISBN(isbn);
    }

    /**
     * Creates a new resource that has yet to be identified by the origin server (i.e., adds a book
     * or replaces and existing one).
     */
    @POST
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response postBooks(@Context UriInfo uriInfo, Book book) {
        log.info("postBooks: {}", book);
        Response response;
        if (ListBookDAO.instance().addBook(book)) {
            response = Response.created(uriInfo.getRequestUriBuilder().path(book.getISBN()).build()).entity(book).build();
        } else {
            response = Response.seeOther(uriInfo.getRequestUriBuilder().path(book.getISBN()).build()).build();
        }
        log.info("[BookResource] postBooks: response status: {} {}", response.getStatus(), response.getStatusInfo());
        return response;
    }

    /**
     * Appends data to a resource's existing representation(s).
     * <br>
     * Treat the addressed member as a collection in its own right and create a new entry within it.
     * <br>
     * <b>Not generally used! Use PATCH instead</b>
     */
    @POST
    @Path("{isbn}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response postBook(@PathParam("isbn") String isbn) {
        return Response.status(Status.METHOD_NOT_ALLOWED).allow("GET", "PUT", "DELETE", "PATCH").build();
    }

    /**
     * Requests that the state of the target resource be created or replaced with the state defined
     * by the representation enclosed in the request message payload.
     * Replace the entire collection
     * <b>In this context having this method makes no sense, because usually one does not replace
     * the entire book collection</b>
     * 
     * @param book
     * @return
     */
    @PUT
    public Response putBooks(List<Book> books) {
        //In this context having this method makes no sense, because usually one does not replace the entire book collection
        return Response.status(Status.METHOD_NOT_ALLOWED).allow("GET", "POST").build();
    }

    /**
     * Requests that the state of the target resource be created or replaced with the state defined
     * by the representation enclosed in the request message payload.
     * <br>
     * Partial content updates are possible by targeting a separately identified resource with state
     * that overlaps a portion of the larger resource, or by using the PATCH method
     * 
     * @param book
     *            book to be replaced
     * @return
     */
    @PUT
    @Path("{isbn}")
    public Response putBook(@PathParam("isbn") String isbn, Book book) {
        //Only the replace functionality is implemented
        Response response;
        if (ListBookDAO.instance().updateBook(isbn, book)) {
            response = Response.noContent().build();
        } else {
            response = Response.status(Status.NOT_FOUND).build();
        }
        return response;
    }

    /**
     * Requests that the origin server remove the association between the target resource and its
     * current functionality.
     * <br>
     * The entire collection is deleted.
     * <b>In this context having this method makes no sense, because usually one does delete the
     * entire book collection</b>
     * 
     * @return
     */
    @DELETE
    public Response deleteBooks() {
        return Response.status(Status.METHOD_NOT_ALLOWED).allow("GET", "POST").build();
    }

    /**
     * Requests that the origin server remove the association between the target resource and its
     * current functionality.
     * <br>
     * The book identified by the URI is removed.
     * 
     * @param isbn
     * @param book
     * @return
     */
    @DELETE
    @Path("{isbn}")
    public Response deleteBook(@PathParam("isbn") String isbn) {
        //Only the replace functionality is implemented
        Response response;
        if (ListBookDAO.instance().deleteBookByISBN(isbn)) {
            response = Response.noContent().build();
        } else {
            response = Response.status(Status.NOT_FOUND).build();
        }
        return response;
    }

    /**
     * Requests that a set of changes described in the request entity be applied to the resource
     * identified by the Request URI.
     * <br>
     * Maybe use it for applying multiple patches on different resources (bulk update).
     * <br>
     * <b>Not generally used!</b>
     * 
     * @return
     */
    @PATCH
    public Response patchBooks() {
        return Response.status(Status.METHOD_NOT_ALLOWED).allow("GET", "POST").build();
    }

    /**
     * Requests that a set of changes described in the request entity be applied to the resource
     * identified by the Request URI.
     * <br>
     * The enclosed entity contains a set of instructions describing how a resource currently
     * residing on the origin server should be modified to produce a new version.
     * <br>
     * Change the title of the book with the specified isbn
     * <br>
     * id, isbn and addedDate cannot be modified
     * 
     * @param isbn
     * @return
     */
    @PATCH
    @Path("{isbn}")
    @Consumes("application/x-www-form-urlencoded")
    public Response patchBook(@PathParam("isbn") String isbn, MultivaluedMap<String, String> map) {
        log.info("patchBook: {}", isbn);
        //Only the replace functionality is implemented
        Response response;
        Book book = ListBookDAO.instance().findBookByISBN(isbn);
        if (book == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        for (Entry<String, List<String>> entry : map.entrySet()) {
            String value = entry.getValue().get(0);
            switch (entry.getKey()) {
                case "author":
                    book.setAuthor(value);
                    break;
                case "title":
                    book.setTitle(value);
                    break;
                case "publisher":
                    book.setPublisher(value);
                    break;
                default:
                    return Response.status(Status.BAD_REQUEST).build();
            }
        }
        if (ListBookDAO.instance().updateBook(isbn, book)) {
            response = Response.noContent().build();
        } else {
            response = Response.status(Status.NOT_FOUND).build();
        }
        return response;
    }
}